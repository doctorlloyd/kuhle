from django.shortcuts import render
from rest_framework.response import Response
from django.http import HttpResponse
from django.views import View
from rest_framework import serializers
from rest_framework.views import APIView
from app import models
import pandas as pd
import json
import requests
from json import dumps
from lxml.html import fromstring
from xmljson import badgerfish as bf


# Create your views here.
class ReferenceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.CodeReference
        fields = ['status_code', 'status_description']

class OrderCustomerSerializer(serializers.HyperlinkedModelSerializer):
    status = ReferenceSerializer(many=False, read_only=False)

    class Meta:
        model = models.Customer
        fields = "__all__"


class ViewOders(APIView):
	serializer_class = OrderCustomerSerializer

	def get_queryset(self):
		queryset = models.Customer.objects.all()
		return queryset


class StatusCode(APIView):
    
	def get(self, *arg, **kwargs):
		response = requests.get('http://197.97.154.196/live/logic/courier_demo')
		status = response.content.decode('utf-8')
		data = dumps(bf.data(fromstring(x)))
		dt = json.loads(data)
		da = dt['span']

		for customer in data:
			model = models.objects.get(customer_code=customer['customercode']['$'])
			try:
				model.customer_code = customer['statuscode']['$']
				save();
			except:
				print('Skipped')

class Customers(APIView):

	def get(self, request, *arg, **kwargs):
		customers = pd.read_csv('http://localhost:8000/static/data.csv', sep=';')
		for index, row in customers.iterrows():
			model = models.Customer()
			model.customer_code = row['customer_code']
			model.customer_first_name = row['customer_first_name']
			model.customer_last_name = row['customer_last_name']
			model.delivery_address = row['delivery_address']
			model.save()

		return Response({'message': 'Orders are logged Successfully'})