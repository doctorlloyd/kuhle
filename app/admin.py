from django.contrib import admin
from app.models import CodeReference, Customer

# Register your models here.
admin.site.register(CodeReference)
admin.site.register(Customer)